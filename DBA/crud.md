# CRUD Operations

## Intro

Create or insert operations add new documents to a collection. If the collection does not currently exist,
 insert operations will create the collection.

MongoDB provides the following methods to insert documents into a collection:

* insertMany()
* inserOne()

Insert operations target a single collection. __All write operations in MongoDB are atomic on the level of a single document__.

![insert]

[insert]:_assets/insert.png

Read operations retrieves documents from a collection; i.e. queries a collection for documents. MongoDB provides the following methods to read documents from a collection:

* find()

![query]

[query]:_assets/query.png

Update operations modify existing documents in a collection. MongoDB provides the following methods to update documents of a collection:

* update()
* updateOne()
* updateMany()
* replaceOne()
* findOneAndReplace()
* findOneAndUpdate()
* findAndModify()
* save()
* bulkWrite()

![update]

[update]:_assets/update.png

Delete operations remove documents from a collection. MongoDB provides the following methods to delete documents of a collection:

* deleteOne()
* deleteMany()
* findOneAndDelete()
* findAndModify()
* bulkWrite()

## Insert

All write operations in MongoDB are atomic on the level of a single document.

With write concerns, you can specify the level of acknowledgement requested from MongoDB for write operations.

## Query

```javascript
db.inventory.find( {} )
db.inventory.find( { status: "D" } )

// Although you can express this query using the $or operator, use the $in operator rather than the $or operator when performing equality checks on the same field.
db.inventory.find( { status: { $in: [ "A", "D" ] } } )
// AND Conditions
db.inventory.find( { status: "A", qty: { $lt: 30 } } )

// OR condition
db.inventory.find( { $or: [ { status: "A" }, { qty: { $lt: 30 } } ] } )

// AND and OR condition
db.inventory.find( {
     status: "A",
     $or: [ { qty: { $lt: 30 } }, { item: /^p/ } ]
} )
```

### Embeded documents

```javascript
db.inventory.insertMany( [
   { item: "journal", qty: 25, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
   { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "A" },
   { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
   { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
   { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" }
]);

// Equality matches on the whole embedded document require an exact match of the specified <value> document, including the field order
db.inventory.find( { size: { h: 14, w: 21, uom: "cm" } } )
//Specify Equality Match on a Nested Field
db.inventory.find( { "size.uom": "in" } )
// Specify Match using Query Operator
db.inventory.find( { "size.h": { $lt: 15 } } )
// AND CONDITION
db.inventory.find( { "size.h": { $lt: 15 }, "size.uom": "in", status: "D" } )
```

### Arrays

```javascript
db.inventory.insertMany([
   { item: "journal", qty: 25, tags: ["blank", "red"], dim_cm: [ 14, 21 ] },
   { item: "notebook", qty: 50, tags: ["red", "blank"], dim_cm: [ 14, 21 ] },
   { item: "paper", qty: 100, tags: ["red", "blank", "plain"], dim_cm: [ 14, 21 ] },
   { item: "planner", qty: 75, tags: ["blank", "red"], dim_cm: [ 22.85, 30 ] },
   { item: "postcard", qty: 45, tags: ["blue"], dim_cm: [ 10, 15.25 ] }
]);
//  queries for all documents where the field tags value is an array with exactly two elements, "red" and "blank", in the specified order
db.inventory.find( { tags: ["red", "blank"] } )
// $ALL - find an array that contains both the elements "red" and "blank", without regard to order or other elements in the array
db.inventory.find( { tags: { $all: ["red", "blank"] } } )
// query if the array field contains at least one element with the specified value
db.inventory.find( { tags: "red" } )
// specify conditions on the elements in the array field,
db.inventory.find( { dim_cm: { $gt: 25 } } )
// queries for documents where the dim_cm array contains elements that in some combination satisfy the query conditions, ONE OF THEM
db.inventory.find( { dim_cm: { $gt: 15, $lt: 20 } } )
// Use $elemMatch operator to specify multiple criteria on the elements of an array such that at least one array element satisfies all the specified criteria.
db.inventory.find( { dim_cm: { $elemMatch: { $gt: 22, $lt: 30 } } } )
// Using dot notation, you can specify query conditions for an element at a particular index or position
db.inventory.find( { "dim_cm.1": { $gt: 25 } } )
//SIZE OEPRATOR - selects documents where the array tags has 3 elements
db.inventory.find( { "tags": { $size: 3 } } )
```

### Array of Embedded Documents

```javascript
db.inventory.insertMany( [
   { item: "journal", instock: [ { warehouse: "A", qty: 5 }, { warehouse: "C", qty: 15 } ] },
   { item: "notebook", instock: [ { warehouse: "C", qty: 5 } ] },
   { item: "paper", instock: [ { warehouse: "A", qty: 60 }, { warehouse: "B", qty: 15 } ] },
   { item: "planner", instock: [ { warehouse: "A", qty: 40 }, { warehouse: "B", qty: 5 } ] },
   { item: "postcard", instock: [ { warehouse: "B", qty: 15 }, { warehouse: "C", qty: 35 } ] }
]);
// Equality matches on the whole embedded/nested document require an exact match of the specified document, including the field order
db.inventory.find( { "instock": { warehouse: "A", qty: 5 } } )
// selects all documents where the instock array has at least one embedded document that contains the field qty whose value is less than or equal to 20
db.inventory.find( { 'instock.qty': { $lte: 20 } } )
// Array Index to Query for a Field in the Embedded Document
db.inventory.find( { 'instock.0.qty': { $lte: 20 } } )
// Single Nested Document Meets Multiple Query Conditions on Nested Fields.
//  queries for documents where the instock array has at least one embedded document that contains both the field qty equal to 5 and the field warehouse equal to A
db.inventory.find( { "instock": { $elemMatch: { qty: 5, warehouse: "A" } } } )
// queries for documents where the instock array has at least one embedded document that contains the field qty equal to 5 and at least one embedded document (but not necessarily the same embedded document) that contains the field warehouse equal to A
db.inventory.find( { "instock.qty": 5, "instock.warehouse": "A" } )
```

### Project fields to return from query

```javascript
db.inventory.insertMany( [
  { item: "journal", status: "A", size: { h: 14, w: 21, uom: "cm" }, instock: [ { warehouse: "A", qty: 5 } ] },
  { item: "notebook", status: "A",  size: { h: 8.5, w: 11, uom: "in" }, instock: [ { warehouse: "C", qty: 5 } ] },
  { item: "paper", status: "D", size: { h: 8.5, w: 11, uom: "in" }, instock: [ { warehouse: "A", qty: 60 } ] },
  { item: "planner", status: "D", size: { h: 22.85, w: 30, uom: "cm" }, instock: [ { warehouse: "A", qty: 40 } ] },
  { item: "postcard", status: "A", size: { h: 10, w: 15.25, uom: "cm" }, instock: [ { warehouse: "B", qty: 15 }, { warehouse: "C", qty: 35 } ] }
]);

db.inventory.find( { status: "A" }, { item: 1, status: 1, _id: 0 } )
// returns all fields except for the status and the instock fields
db.inventory.find( { status: "A" }, { status: 0, instock: 0 } )ç
db.inventory.find(
   { status: "A" },
   { item: 1, status: 1, "size.uom": 1 }
)
db.inventory.find( { status: "A" }, { item: 1, status: 1, "instock.qty": 1 } )
// MongoDB provides the following projection operators for manipulating arrays: $elemMatch, $slice, and $
db.inventory.find( { status: "A" }, { item: 1, status: 1, instock: { $slice: -1 } } )
```

### Query for Null or Missing Fields

```javascript
db.inventory.insertMany([
   { _id: 1, item: null },
   { _id: 2 }
])
//  returns both documents in the collection
db.inventory.find( { item: null } )
// returns only the document where the item field has a value of null
db.inventory.find( { item : { $type: 10 } } )
// query matches documents that do not contain the item field
db.inventory.find( { item : { $exists: false } } )
```

### Iterate a Cursor in the mongo Shell

The db.collection.find() method returns a cursor. To access the documents, you need to iterate the cursor. However, in the mongo shell, if the returned cursor is not assigned to a variable using the var keyword, then the cursor is automatically iterated up to 20 times [1] to print up to the first 20 documents in the results.

```javascript
var myCursor = db.users.find( { type: 2 } );

while (myCursor.hasNext()) {
   //print(tojson(myCursor.next()));
   printjson(myCursor.next());
}

var myCursor = db.inventory.find( { type: 2 } );
var documentArray = myCursor.toArray();
var myDocument = documentArray[3];
```

#### Cursor Information

The db.serverStatus() method returns a document that includes a metrics field. The metrics field contains a metrics.cursor field with the following information:

```javascript
db.serverStatus().metrics.cursor

{
   "timedOut" : <number> of timed out cursors since the last server restart
   "open" : {
      "noTimeout" : <number of open cursors with the option DBQuery.Option.noTimeout set to prevent timeout after a period of inactivity>,
      "pinned" : <number of “pinned” open cursors>,
      "total" : <number of open cursors>
   }
}
```

## Update

* If an update operation changes the document size, the operation will fail.
* updateMany() is not compatible with db.collection.explain(). Use update() instead.
* If the operation results in an upsert, the collection must already exist.
* db.collection.updateMany() supports multi-document transactions [4.0] (next chapters). Do not explicitly set the write concern for the operation if run in a transaction.

```java
// All update() operations for a sharded collection that specify the justOne option must include the shard key or the _id field in the query specification. update() operations specifying justOne in a sharded collection which do not contain either the shard key or the _id field return an error.

// db.collection.update() supports multi-document transactions
db.collection.update(
   <query>,
   <update>,
   {
     upsert: <boolean>,
     multi: <boolean>,
     writeConcern: <document>,
     collation: <document>,
     arrayFilters: [ <filterdocument1>, ... ]
   }
)

db.collection.updateMany(
   <filter>,
   <update>,
   {
     upsert: <boolean>,
     writeConcern: <document>, // document expressing the write concern.
     collation: <document>, // allows users to specify language-specific rules for string comparison, such as rules for lettercase and accent marks.
     arrayFilters: [ <filterdocument1>, ... ] // array of filter documents that determines which array elements to modify for an update operation on an array field.
   }
)

db.collection.replaceOne(
   <filter>,
   <replacement>,
   {
     upsert: <boolean>,
     writeConcern: <document>,
     collation: <document>
   }
)

// Collation
collation: {
   locale: <string>,
   caseLevel: <boolean>,
   caseFirst: <string>,
   strength: <int>,
   numericOrdering: <boolean>,
   alternate: <string>,
   maxVariable: <string>,
   backwards: <boolean>
}

```

If updateOne(), updateMany(), or replaceOne() includes upsert : true and no documents match the specified filter, then the operation creates a new document and inserts it.

With write concerns, you can specify the level of acknowledgement requested from MongoDB for write operations.

```javascript
db.inventory.insertMany( [
   { item: "canvas", qty: 100, size: { h: 28, w: 35.5, uom: "cm" }, status: "A" },
   { item: "journal", qty: 25, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
   { item: "mat", qty: 85, size: { h: 27.9, w: 35.5, uom: "cm" }, status: "A" },
   { item: "mousepad", qty: 25, size: { h: 19, w: 22.85, uom: "cm" }, status: "P" },
   { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "P" },
   { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
   { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
   { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" },
   { item: "sketchbook", qty: 80, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
   { item: "sketch pad", qty: 95, size: { h: 22.85, w: 30.5, uom: "cm" }, status: "A" }
] );

// Update single docuemnt
db.inventory.updateOne(
   { item: "paper" },
   {
     $set: { "size.uom": "cm", status: "P" },
     // update the value of the lastModified field to the current date. If lastModified field does not exist, $currentDate will create the field.
     $currentDate: { lastModified: true }
   }
)

try {
   db.restaurant.updateOne(
       { item: "paper" },,
       { $inc: { "qty" : 3}, $set: { "status" : "C" } },
       { w: "majority", wtimeout: 100 }
   );
} catch (e) {
   print(e);
}

// Update Multiple Documents
db.inventory.updateMany(
   { "qty": { $lt: 50 } },
   {
     $set: { "size.uom": "in", status: "P" },
     $currentDate: { lastModified: true }
   }
)

// delete field
db.books.update( { _id: 1 }, { $unset: { tags: 1 } } )

// Replace a document
db.inventory.replaceOne(
   { item: "paper" },
   { item: "paper", instock: [ { warehouse: "A", qty: 60 }, { warehouse: "B", qty: 40 } ] }
)

// upsert
try {
   db.inspectors.updateMany(
      { "Sector" : { $gt : 4 }, "inspector" : "R. Coltrane" },
      { $set: { "Patrolling" : false } },
      { upsert: true }
   );
} catch (e) {
   print(e);
}

// majority
try {
   db.restaurant.updateMany(
       { "name" : "Pizza Rat's Pizzaria" },
       { $inc: { "violations" : 3}, $set: { "Closed" : true } },
       { w: "majority", wtimeout: 100 }
   );
} catch (e) {
   print(e);
}
// result:
WriteConcernError({
   "code" : 64,
   "errInfo" : {
      "wtimeout" : true
   },
   "errmsg" : "waiting for replication timed out"
}) :
undefined

// collation
db.myColl.updateMany(
   { category: "cafe" },
   { $set: { status: "Updated" } },
   { collation: { locale: "fr", strength: 1 } }
);

// array filters
db.students.insert([
   { "_id" : 1, "grades" : [ 95, 92, 90 ] },
   { "_id" : 2, "grades" : [ 98, 100, 102 ] },
   { "_id" : 3, "grades" : [ 95, 110, 100 ] }
])

// return the 2 first documents
db.students.updateMany(
   { grades: { $gte: 100 } },
   { $set: { "grades.$[element]" : 100 } },
   { arrayFilters: [ { "element": { $gte: 100 } } ] }
)

// Update method example
db.books.update(
   { stock: { $lte: 10 } },
   { $set: { reorder: true } },
   {
     multi: true,
     writeConcern: { w: "majority", wtimeout: 5000 }
   }
)
```

## Delete

* Delete operations do not drop indexes, even if deleting all documents from a collection.
* All write operations in MongoDB are atomic on the level of a single document.
* With write concerns, you can specify the level of acknowledgement requested from MongoDB for write operations.

```javascript
db.inventory.insertMany( [
   { item: "journal", qty: 25, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
   { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "P" },
   { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
   { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
   { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" },
] );

db.inventory.deleteMany({})
db.inventory.deleteMany({ status : "A" })
db.inventory.deleteOne( { status: "D" } )
```

## Bulk operations

* Bulk write operations affect a single collection. Bulk write operations can be either ordered (serially and failing in error) or unordered (opposite).
* __db.collection.bulkWrite()__ method provides the ability to perform bulk insert, update, and remove operations. MongoDB also supports bulk insert through the __db.collection.insertMany()__.
* Executing an ordered list of operations on a sharded collection will generally be slower than executing an unordered list since with an ordered list, each operation must wait for the previous operation to finish.

```javascript
{ "_id" : 1, "char" : "Brisbane", "class" : "monk", "lvl" : 4 },
{ "_id" : 2, "char" : "Eldon", "class" : "alchemist", "lvl" : 3 },
{ "_id" : 3, "char" : "Meldane", "class" : "ranger", "lvl" : 3 }
```

```javascript
try {
   db.characters.bulkWrite(
      [
        { insertOne :
          {
            "document" :
            {
              "_id" : 4, "char" : "Dithras", "class" : "barbarian", "lvl" : 4
            }
          }
        },
        { insertOne :
          {
            "document" :
            {
              "_id" : 5, "char" : "Taeln", "class" : "fighter", "lvl" : 3
            }
          }
        },
        { updateOne :
          {
            "filter" : { "char" : "Eldon" },
            "update" : { $set : { "status" : "Critical Injury" } }
          }
        },
        { deleteOne :
          { "filter" : { "char" : "Brisbane"} }
        },
        { replaceOne :
          {
            "filter" : { "char" : "Meldane" },
            "replacement" : { "char" : "Tanys", "class" : "oracle", "lvl" : 4 }
          }
        }
      ]
   );
}
catch (e) {
   print(e);
}


// RESULT
{
   "acknowledged" : true,
   "deletedCount" : 1,
   "insertedCount" : 2,
   "matchedCount" : 2,
   "upsertedCount" : 0,
   "insertedIds" : {
      "0" : 4,
      "1" : 5
   },
   "upsertedIds" : {

   }
}
```

### Bulk strategies in a sharded collection

* Pre-Split the Collection with _sh.splitFind_ or _sh.splitAt_.
* Unordered Writes to mongos: improve write performance to sharded clusters, use _bulkWrite()_ with the optional parameter ordered set to false.
* Avoid Monotonic Throttling.

## Avoid Monotonic Throttling

Retryable writes allow MongoDB drivers to automatically retry certain write operations a single time if they encounter network errors, or if they cannot find a healthy primary in the replica sets or sharded cluster.

[4.0] The transaction commit and abort operations are retryable write operations. If the commit operation or the abort operation encounters an error, MongoDB drivers retry the operation a single time regardless of whether retryWrites is set to true.

```zsh
mongodb://localhost/?retryWrites=true

mongo --retryWrites
```

Write operations are retryable when issued with acknowledged write concern; e.g., Write Concern cannot be {w: 0}

## Text search

```javascript
db.stores.insert(
   [
     { _id: 1, name: "Java Hut", description: "Coffee and cakes" },
     { _id: 2, name: "Burger Buns", description: "Gourmet hamburgers" },
     { _id: 3, name: "Coffee Shop", description: "Just coffee" },
     { _id: 4, name: "Clothes Clothes Clothes", description: "Discount clothing" },
     { _id: 5, name: "Java Shopping", description: "Indonesian goods" }
   ]
)

// To perform text search queries, you must have a text index on your collection. A collection can only have one text search index, but that index can cover multiple fields
db.stores.createIndex( { name: "text", description: "text" } )
db.stores.find( { $text: { $search: "java coffee shop" } } )
// search for exact phrases
db.stores.find( { $text: { $search: "java \"coffee shop\"" } } )
// To exclude a word, you can prepend a “-” characte
db.stores.find( { $text: { $search: "java shop -coffee" } } )
// sort the results in order of relevance score, using $meta to obtain and sort by the relevance score of each matching document.
db.stores.find(
   { $text: { $search: "java coffee shop" } },
   { score: { $meta: "textScore" } }
).sort( { score: { $meta: "textScore" } } )

//Using aggregation framework
db.articles.aggregate(
   [
     { $match: { $text: { $search: "cake" } } },
     { $group: { _id: null, views: { $sum: "$views" } } }
   ]
)

db.articles.aggregate(
   [
     { $match: { $text: { $search: "cake tea" } } },
     { $sort: { score: { $meta: "textScore" } } },
     { $project: { title: 1, _id: 0 } }
   ]
)

db.articles.aggregate(
   [
     { $match: { $text: { $search: "cake tea" } } },
     { $project: { title: 1, _id: 0, score: { $meta: "textScore" } } },
     { $match: { score: { $gt: 1.0 } } }
   ]
)

db.articles.aggregate(
   [
     { $match: { $text: { $search: "saber -claro", $language: "es" } } },
     { $group: { _id: null, views: { $sum: "$views" } } }
   ]
)
```

## Geospatial queries

To calculate geometry over an Earth-like sphere store your location data as GeoJSON objects.

```javascript
<field>: { type: <GeoJSON type> , coordinates: <coordinates> }

location: {
      type: "Point",
      coordinates: [-73.856077, 40.848447]
}

db.collection.createIndex( { <location field> : "2dsphere" } )
```

### Indexes

Geospatial indexes cannot cover a query.

```javascript
// earth-like spheres
db.collection.createIndex( { <location field> : "2dsphere" } )
// 2d plane
db.collection.createIndex( { <location field> : "2d" } )
```

The following geospatial operations are supported on sharded collections:

* __\$geoNear__ aggregation stage
* __\$near__ and __\$nearSphere__ query operators (starting in MongoDB 4.0)
* __\$geoNear__ command [4.0] __Deprecated__

```javascript
db.places.insert( {
    name: "Central Park",
   location: { type: "Point", coordinates: [ -73.97, 40.77 ] },
   category: "Parks"
} );
db.places.insert( {
   name: "Sara D. Roosevelt Park",
   location: { type: "Point", coordinates: [ -73.9928, 40.7193 ] },
   category: "Parks"
} );
db.places.insert( {
   name: "Polo Grounds",
   location: { type: "Point", coordinates: [ -73.9375, 40.8303 ] },
   category: "Stadiums"
} );

db.places.find(
   {
     location:
       { $near:
          {
            $geometry: { type: "Point", coordinates: [ -73.9667, 40.78 ] },
            $minDistance: 1000,
            $maxDistance: 5000
          }
       }
   }
)

db.places.aggregate( [
   {
      $geoNear: {
         near: { type: "Point", coordinates: [ -73.9667, 40.78 ] },
         spherical: true,
         query: { category: "Parks" },
         distanceField: "calcDistance"
      }
   }
] )
```

## Read Concern

The readConcern option allows you to control the consistency and isolation properties of the data read from replica sets and replica set shards.

```javascript
db.restaurants.find( { _id: 5 } ).readConcern("linearizable").maxTimeMS(10000)

db.runCommand( {
     find: "restaurants",
     filter: { _id: 5 },
     readConcern: { level: "linearizable" }, // local, available, majority and linearizable
     maxTimeMS: 10000
} )
```

* __local__: A query with read concern "local" returns data from the instance with no guarantee that the data has been written to a majority of the replica set members. It's the default for reads against primary or reads against secondaries if the reads are associated with causally __consistent sessions__.
* __available__: A query with read concern “available” returns data from the instance with no guarantee that the data has been written to a majority of the replica set members. For sharded collections, "available" read concern provides the lowest latency reads possible among the various read concerns but at the expense of consistency as "available" read concern can return orphaned documents (chunk migration). It's the default for reads against secondaries if the reads are not associated with causally consistent sessions (not allowed).
* __majority__: The query returns the data that has been acknowledged by a majority of the replica set members. The documents returned by the read operation are durable, even in the event of failure. We must to use WiredTiger. Read concern majority is available for use with causally consistent sessions and transactions.
* __linearizable__: The query returns data that reflects all successful majority-acknowledged writes that completed prior to the start of the read operation. The query may wait for concurrently executing writes to propagate to a majority of replica set members before returning results. You can specify linearizable read concern for read operations on the primary only.
* __snapshot__: Read concern "snapshot" is only available for multi-document transactions. Multi-document transactions support read concern "snapshot" as well as "local", and "majority".

### Consistent sessions

To provide causal consistency, MongoDB 3.6 enables causal consistency in client sessions. A causally consistent session denotes that the associated sequence of read operations with "majority" read concern and write operations with "majority" write concern have a causal relationship that is reflected by their ordering. Applications must ensure that only one thread at a time executes these operations in a client session.

If an operation logically depends on a preceding operation, there is a causal relationship between the operations. For example, a write operation that deletes all documents based on a specified condition and a subsequent read operation that verifies the delete operation have a causal relationship.

### Transactions and read concern

For "local" and "majority" read concern, MongoDB may provide stronger isolation guarantees than specified. Specifically, in MongoDB [4.0], all multi-documents transactions have "snapshot" isolation.

## Write concern

Write concern describes the level of acknowledgement requested from MongoDB for write operations to a standalone mongod or to replica sets or to sharded clusters. In sharded clusters, mongos instances will pass the write concern on to the shards.

```javascript
{ w: <value>, j: <boolean>, wtimeout: <number> }
```

With j: true, MongoDB returns only after the requested number of members, including the primary, have written to the journal (previously j: true write concern in a replica set only requires the primary to write to the journal, regardless of the w: <value> write concern).

## Atomicity and transactions

In MongoDB, a write operation is atomic on the level of a single document, even if the operation modifies multiple embedded documents within a single document.

## Multi-Document Transactions

When a single write operation (e.g. db.collection.updateMany()) modifies multiple documents, the modification of each document is atomic, but the operation as a whole is not atomic.

Starting in version [__4.0__], for situations that require atomicity for updates to multiple documents or consistency between reads to multiple documents, MongoDB provides __multi-document transactions for replica sets__.

## Concurrency Control

Concurrency control allows multiple applications to run concurrently without causing data inconsistency or conflicts.

## Distributed queries

By default, clients reads from a replica set’s primary; however, clients can specify a read preference to direct read operations to other members. For example, clients can configure read preferences to read from secondaries or from nearest member to:

* reduce latency in multi-data-center deployments.
* improve read throughput by distributing high read-volumes (relative to write volume).
* perform backup operations
* allow reads until a new primary is elected.

In replica sets, all write operations go to the set’s primary. The primary applies the write operation and records the operations on the primary’s operation log or oplog. The oplog is a reproducible sequence of operations to the data set. Secondary members of the set continuously replicate the oplog and apply the operations to themselves in an asynchronous process.

Read operations on sharded clusters are most efficient when directed to a specific shard. Queries to sharded collections should include the collection’s shard key.  When a query includes a shard key, the mongos can use cluster metadata from the config database to route the queries to shards.

If a query does not include the shard key, the mongos must direct the query to all shards in the cluster. These __scatter gather__ queries can be inefficient.

For sharded collections in a sharded cluster, the mongos directs write operations from applications to the shards that are responsible for the specific portion of the data set. The mongos uses the cluster metadata from the config database to route the write operation to the appropriate shards.

MongoDB partitions data in a sharded collection into ranges based on the values of the shard key. Then, MongoDB distributes these chunks to shards. The shard key determines the distribution of chunks to shards. This can affect the performance of write operations in the cluster.

If the value of the shard key increases or decreases with every insert, all insert operations target a single shard. As a result, the capacity of a single shard becomes the limit for the insert capacity of the sharded cluster.

## Query optimization


# Mongo shell

The mongo shell is an interactive JavaScript interface to MongoDB.

## How to connect

```javascript
// Connect to a singleton with user pwd
mongo --username alice --password --authenticationDatabase admin --host mongodb0.examples.com --port 28015
// Connect to a replica set named replA:
mongo --host replA/mongodb0.example.com.local:27017,mongodb1.example.com.local:27017,mongodb2.example.com.local:27017
// TLS/SSL connections
mongo --ssl --host replA/mongodb0.example.com.local:27017,mongodb1.example.com.local:27017,mongodb2.example.com.local:27017
```

## Configure mongo shell

```javascript
// configure prompt
cmdCount = 1;
prompt = function() {
             return (cmdCount++) + "> ";
         }

host = db.serverStatus().host;
prompt = function() {
             return db+"@"+host+"$ ";
         }

prompt = function() {
           return "Uptime:"+db.serverStatus().uptime+" Documents:"+db.stats().objects+" > ";
         }

// Change mongo shell batch size
DBQuery.shellBatchSize = 10;
```

## Writting scripts

You can write scripts for the mongo shell in JavaScript that manipulate data in MongoDB or perform administrative operation.

```javascript
conn = new Mongo();
db = conn.getDB("myDatabase");

//If connecting to a MongoDB instance that enforces access control,
// you can use the db.auth() method to authenticate.

```

When writing scripts for the mongo shell, consider the following:

* To set the db global variable, __use the getDB() method or the connect() method__. You can assign the database reference to a variable other than db.
* Write operations in the mongo shell use a __write concern of { w: 1 } by default__. If performing bulk operations, use the Bulk() methods. See Write Method Acknowledgements for more information.
* You cannot use any shell helper (e.g. use <dbname>, show dbs, etc.) inside the JavaScript file because they are not valid JavaScript.

```javascript
// Shell helpers vs JS equivalents
show dbs, show databases => db.adminCommand('listDatabases')
use <db> => db = db.getSiblingDB('<db>')
show collections => db.getCollectionNames()
show users => db.getUsers()
show roles => db.getRoles({showBuiltinRoles: true})
show log <logname> => db.adminCommand({ 'getLog' : '<logname>' })
show logs => db.adminCommand({ 'getLog' : '*' })
it => cursor = db.collection.find()
if ( cursor.hasNext() ){
   cursor.next();
}
```

From the system prompt, use mongo to evaluate JavaScript.

```javascript
mongo test --eval "printjson(db.getCollectionNames())"
```

### Mongo shell types

* Date

```javascript
// return the date as a string, use the Date()
var myDateString = Date();

// As date
var myDate = new Date();
var myDateInitUsingISODateWrapper = ISODate();
myDate instanceof Date
myDateInitUsingISODateWrapper instanceof Date
```

* ObjectId
* NumberLong

```javascript
db.collection.insertOne( { _id: 10, calc: NumberLong("2090845886852") } )
db.collection.updateOne( { _id: 10 },
                      { $set:  { calc: NumberLong("2555555000000") } } )
db.collection.updateOne( { _id: 10 },
                      { $inc: { calc: NumberLong(5) } } )
```

* NumberInt
* NumberDecimal: [3.4] The mongo shell treats all numbers as 64-bit floating-point double values by default. The mongo shell provides the NumberDecimal() constructor to explicitly specify 128-bit decimal-based floating-point values capable of emulating decimal rounding with exact precision.

# Introduction

## Databases and collection

- Review [collMod](https://docs.mongodb.com/manual/reference/command/collMod/#dbcmd.collMod), it makes it possible to add options to a collection or to modify view definitions. The command takes the following prototype form:

```javascript
db.runCommand( { collMod: <collection or view>, <option1>: <value1>, <option2>: <value2> ... } )
```

```javascript
db.runCommand( { collMod: "contacts",
   validator: { $jsonSchema: {
      bsonType: "object",
      required: [ "phone" ],
      properties: {
         phone: {
            bsonType: "string",
            description: "must be a string and is required"
         },
         email: {
            bsonType : "string",
            pattern : "@mongodb\.com$",
            description: "must be a string and match the regular expression pattern"
         },
         status: {
            enum: [ "Unknown", "Incomplete" ],
            description: "can only be one of the enum values"
         }
      }
   } },
   validationLevel: "moderate",
   validationAction: "warn"
} )
```

- Review [Document and schema validation](https://docs.mongodb.com/manual/core/schema-validation/)
  
    Let's see a sample:

```javascript
db.createCollection("students", {
   validator: {
      $jsonSchema: {
         bsonType: "object",
         required: [ "name", "year", "major", "gpa" ],
         properties: {
            name: {
               bsonType: "string",
               description: "must be a string and is required"
            },
            gender: {
               bsonType: "string",
               description: "must be a string and is not required"
            },
            year: {
               bsonType: "int",
               minimum: 2017,
               maximum: 3017,
               exclusiveMaximum: false,
               description: "must be an integer in [ 2017, 3017 ] and is required"
            },
            major: {
               enum: [ "Math", "English", "Computer Science", "History", null ],
               description: "can only be one of the enum values and is required"
            },
            gpa: {
               bsonType: [ "double" ],
               minimum: 0,
               description: "must be a double and is required"
            }
         }
      }
   }
})
```

```javascript
db.createCollection( "contacts",
   { validator: { $or:
      [
         { phone: { $type: "string" } },
         { email: { $regex: /@mongodb\.com$/ } },
         { status: { $in: [ "Unknown", "Incomplete" ] } }
      ]
   }
} )
```

### Views

(New in 3.4)

adds support for creating read-only views from existing collections or other views. __Views are read-only; write operations on views will error__.

- Using _runCommand_

```javascript
// collation (language-specific rules for string comparison) is optional
db.runCommand( { create: <view>, viewOn: <source>, pipeline: <pipeline>, collation: <collation> } )
```

- New mongoshell createView:

```javascript
db.createView(<view>, <source>, <pipeline>, <collation> )
```

it supports the following operations:

- db.collection.find()
- db.collection.findOne()
- db.collection.aggregate()
- db.collection.count()
- db.collection.distinct()

It doesn't supports the following projection operators:

- $
- $elemMatch
- $slice
- $meta

### Capped collections

fixed-size collections that support high-throughput operations that insert and retrieve documents based on insertion order. Capped collections work in a way similar to circular buffers: once a collection fills its allocated space, it makes room for new documents by overwriting the oldest documents in the collection.

__If an update or a replacement operation changes the document size, the operation will fail. You cannot delete documents from a capped collection. You cannot shard a capped collection.__

```javascript
//create
db.createCollection("log", { capped : true, size : 5242880, max : 5000 } )
db.collection.isCapped()
//query reverse insertion order
db.cappedCollection.find().sort( { $natural: -1 } )
// convert to capped
db.runCommand({"convertToCapped": "mycoll", size: 100000});
```

As an alternative to capped collections, consider MongoDB’s __TTL (“time to live”) indexes__.

## Documents

MongoDB stores data records as BSON documents. BSON is a binary representation of JSON documents, though it contains more data types than JSON. See [BSON types](https://docs.mongodb.com/manual/reference/bson-types/).

```javascript
var mydoc = {
            _id: ObjectId("5099803df3f4948bd2f98391"),
            name: { first: "Alan", last: "Turing" },
            birth: new Date('Jun 23, 1912'),
            death: new Date('Jun 07, 1954'),
            contribs: [ "Turing machine", "Turing test", "Turingery" ],
            views : NumberLong(1250000)
        }
```

### Dot Notation

MongoDB uses the dot notation to access the elements of an array and to access the fields of an embedded document. To specify or access an element of an array by the zero-based index position, concatenate the array name with the dot (.) and zero-based index position, and enclose in quotes:

```javascript
{
   ...
   contribs: [ "Turing machine", "Turing test", "Turingery" ],
   ...
}

contribs.2
```

To specify or access a field of an embedded document with dot notation, concatenate the embedded document name with the dot (.) and the field name, and enclose in quotes:

```javascript
{
   ...
   name: { first: "Alan", last: "Turing" },
   contact: { phone: { type: "cell", number: "111-222-3333" } },
   ...
}

name.last
```

### Document limitations

__The maximum BSON document size is 16 megabytes.__

The maximum document size helps ensure that a single document cannot use excessive amount of RAM or, during transmission, excessive amount of bandwidth. To store documents larger than the maximum size, MongoDB provides the GridFS API.

__Document Field Order__

MongoDB preserves the order of the document fields following write operations except for the following cases:

* _id field is always the first field in the document.
* Updates that include renaming of field names may result in the reordering of fields in the document.

__id Field__

Each document stored in a collection requires a unique _id field that acts as a primary key. If an inserted document omits the _id field, the MongoDB driver automatically generates an ObjectId for the _id field.

This also applies to documents inserted through update operations with upsert: true.

Common options for storing values for _id:

* Use an ObjectId.
* Use a natural unique identifier, if available.
* Generate an auto-incrementing number. (Bad idea for sharding)
* Generate a UUID in your application code. For a more efficient storage of the UUID values in the collection and in the _id index, store the UUID as a value of the BSON BinData type.
* Use your driver’s BSON UUID facility to generate UUIDs.

## BSON

MongoDB stores data records as BSON documents. BSON is a binary representation of JSON documents, though it contains more data types than JSON.

MongoDB documents are composed of field-and-value pairs. The value of a field can be any of the BSON data types, including other documents, arrays, and arrays of documents.

```javascript
var mydoc = {
            _id: ObjectId("5099803df3f4948bd2f98391"),
            name: { first: "Alan", last: "Turing" },
            birth: new Date('Jun 23, 1912'),
            death: new Date('Jun 07, 1954'),
            contribs: [ "Turing machine", "Turing test", "Turingery" ],
            views : NumberLong(1250000)
        }
```

BSON documents may have more than one field with the same name. Most MongoDB interfaces, however, represent MongoDB with a structure (e.g. a hash table) that does not support duplicate field names.

### Document Limitations

__Maximum BSON document size__ 

It's 16 megabytes. The maximum document size helps ensure that a single document cannot use excessive amount of RAM or, during transmission, excessive amount of bandwidth. To store documents larger than the maximum size, MongoDB provides the __GridFS API__.

__Document Field Order__

MongoDB preserves the order of the document fields following write operations except for the following cases:

- _id field is always the first field.
- Updates that include renaming of field names may result in the reordering of fields in the document.

### Uses of the Document Structure

In addition to defining data records, MongoDB uses the document structure throughout, including but not limited to:

- Query Filter Documents: specify the conditions that determine which records to select for read, update, and delete operations.
- Update Specification Documents: use update operators to specify the data modifications to perform on specific fields during an db.collection.update() operation.
- Index Specification Documents.

### BSON types

Each BSON type has both integer and string identifiers as listed in the following table:

![BSON types]

[BSON types]:_assets/bson-types.png

## Comparison, sort and order

When comparing values of different BSON types, MongoDB uses the following comparison order, from lowest to highest:

![Comparison]

[Comparison]:_assets/comparison.png

### Collation

Collation allows users to specify language-specific rules for string comparison, such as rules for lettercase and accent marks.

Collation specification has the following syntax:

```javascript
{
   locale: <string>,
   caseLevel: <boolean>,
   caseFirst: <string>,
   strength: <int>,
   numericOrdering: <boolean>,
   alternate: <string>,
   maxVariable: <string>,
   backwards: <boolean>
}
```

When specifying collation, __the locale field is mandatory__.

### Arrays

With arrays, a less-than comparison or an ascending sort compares the smallest element of arrays, and a greater-than comparison or a descending sort compares the largest element of the arrays.

### Objects

MongoDB’s comparison of BSON objects uses the following order:

1. Recursively compare key-value pairs in the order that they appear within the BSON object.
2. Compare the key field names.
3. If the key field names are equal, compare the field values.
4. If the field values are equal, compare the next key/value pair (return to step 1). An object without further pairs is less than an object with further pairs.

### Date and timestamps

Date objects sort before Timestamp objects [3.0.0]. Previously Date and Timestamp objects sorted together.

## MongoDB Extended JSON

JSON can only represent a subset of the types supported by BSON. To preserve type information, MongoDB adds the following extensions to the JSON format:

* __Strict mode__. Strict mode representations of BSON types conform to the JSON RFC. Any JSON parser can parse these strict mode representations as key/value pairs; however, only the MongoDB internal JSON parser recognizes the type information conveyed by the format.
* __mongo Shell mode__. The MongoDB internal JSON parser and the mongo shell can parse this mode.

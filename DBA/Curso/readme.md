# Curso Mongo

## Contenido

1. Introducción

2. Storage engines

3. Data modeling

4. CRUD
	4.1 Demo

5. Indexes
	5.1 Demo

6. Replica set
	6.1 Demo

7. Sharding
	7.1 Demo


## Introducción - https://docs.mongodb.com/manual/introduction/	(2 diapositivas)
	* Base de datos orientada a documentos
		* JSON
		* BSON
			- BSON max size 16MB (GridFS)
			- $type
			- Comparison, Sort order - https://docs.mongodb.com/manual/reference/bson-type-comparison-order/
	* High perfomance
		* Documentos embebidos para reducir operaciones I/O
		* Índices para reducir el tiempo de respuesta en las queries
	* High availability & Scalability

## Storage engines
	* MMAPv1
	* WiredTiger
	* In-Memory

## Data modeling

## CRUD
	* Read
	* Write
	* Performance
	* Práctica: Starting a single node instance using command-line options
	* Práctica: Single node installation of MongoDB with options from the config file
	* Práctica: Connecting to a single node from the Mongo shell with a preloaded JavaScript
	* Práctica: Creating test data (mongoimport)
	* Práctica: Performing simple querying, projections,and pagination from the Mongo shell
	* Práctica: Updating and deleting data from the shell

## Indexes
	* Tipos
	* Performance
	* Práctica: Creating an index and viewing plans of queries
	* Práctica: Background and foreground index creation from the shell

## Replica
	* Performance
	* Automathic failover
	* Práctica: Starting multiple instances as part of a replica set
	* Práctica: Configuring a replica set
	* Práctica: Connecting to the replica set from the shell to query and insert data

## Sharding
	* Performance
	* Práctica: Starting a simple sharded environment of two shards
	* Práctica: Connecting to a shard from the Mongo shell and performing operations



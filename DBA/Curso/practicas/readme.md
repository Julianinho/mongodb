# Prácticas del curso

## Instalación

```zsh
// Import the public key used by the package management system
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5

// Creamos el fichero /etc/apt/sources.list.d/mongodb-org-3.6.list según la versión del SO.
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list

// Recargamos
sudo apt-get update

// Instalamos
sudo apt-get install -y mongodb-org
sudo mkdir /data/mongo/db
sudo chmod -R 755 /data/mongo/db
sudo chown -R 'username' /data/mongo/db

// Versión concreta
sudo apt-get install -y mongodb-org=3.6.1 mongodb-org-server=3.6.1 mongodb-org-shell=3.6.1 mongodb-org-mongos=3.6.1 mongodb-org-tools=3.6.1

// Desinstalación
sudo apt-get purge mongodb-org*
sudo rm -r /var/log/mongodb
sudo rm -r /var/lib/mongodb

// Comprobar disponibilidad de puerto
sudo netstat -tlnp | grep 27017

// Iniciamos el servidor
mkdir /logs
mkdir /data/mongo/pid
sudo touch /logs/mongodb.log
sudo touch /data/mongo/pid/mongod.pid
sudo chmod 755 mongodb.log
chown -R 'username' /logs
chown -R 'username' /data

// Alternativamente, usándolo como service del systemctl
sudo systemctl enable mongod
service mongod start
service mongod status
service mongod stop

// Fichero de configuración usado por el servicio
nano /etc/mongod.conf


// Iniciamos el server
mongod --dbpath /data/mongo/db --port 27017 --fork --logpath /logs/mongodb.log --logappend

// Comprobamos
ps -edaf | grep mongo | grep -v grep

```

### Config file

```zsh
kill <pid>
mongod --help
mongod --config ~/conf/mongod.yaml
```

### Mongo shell with javascript

```zsh
mongo --shell ~/Repositorios/Julianinho/mongodb/mongoshell_scripts/homework.js
homework.init()
t.find({}).pretty()
homework.close()
```

## CRUD

### Insert and mongoimport

```zsh
 mongoimport -c people -d tests --drop --file ~/conf/people.json
 mongoimport -c grades -d tests --file ~/conf/grades.json
 mongoimport -c students -d tests --file ~/conf/students.json

// Avoid using mongoimport and mongoexport for full instance production backups. They do not reliably preserve all rich BSON data types, because JSON can only represent a subset of the types supported by BSON. Use mongodump and mongorestore as described in MongoDB Backup Methods for this kind of functionality.

mongo
use tests
show collections
var people = db.people
people.insert({_id:'ThisIsFake', address:1, onefield: 4.75})
people.find({_id:'ThisIsFake'})

// On robomongo
db.getCollection('people').find({}).sort({_id:1})
```

### Queries

```zsh
people.count()
people.findOne()
people.find().pretty()
people.find({last_name:"Smith"}).count()

// Some simple queries

people.find(
	{last_name:"Smith", company_id: ObjectId("57d7a121fa937f710a7d486d")}
).count()

people.find(
	{$or:[{last_name:"Smith"}, {company_id: ObjectId("57d7a121fa937f710a7d486d")}]}
).count()

people.find(
	{last_name: { $in: ["Smith", "Herrera"] }}
).count()

people.find(
	{last_name: {$gt: "Smith"}}
)

people.find(
	{_id:{$type: 2}}
)

people.find(
	{onefield:{$exists:true}}
)


// Query modifiers
people.find(
	{last_name: {$gt: "Smith"}}
).sort({last_name:1}).skip(20).limit(10).pretty()

// Embedded document
people.find(
	{address:{"city" : "Dukemouth","street" : "96735 Williamson Forks","zip" : "76452"}}
).pretty()

//Dot notation
people.find({"address.city": "Austinport"}).pretty()

//Projection
people.find({},{_id:0,"address.city":1}).sort({"address.city":1})

// Arrays
db.inventory.insertMany([
   { item: "journal", qty: 25, tags: ["blank", "red"], dim_cm: [ 14, 21 ] },
   { item: "notebook", qty: 50, tags: ["red", "blank"], dim_cm: [ 14, 21 ] },
   { item: "paper", qty: 100, tags: ["red", "blank", "plain"], dim_cm: [ 14, 21 ] },
   { item: "planner", qty: 75, tags: ["blank", "red"], dim_cm: [ 22.85, 30 ] },
   { item: "postcard", qty: 45, tags: ["blue"], dim_cm: [ 10, 15.25 ] }
]);

//Exactly two elements, with this values and order
db.inventory.find( { tags: ["red", "blank"] } )

//Que contenga los dos, independientemente del orden
db.inventory.find( { tags: { $all: ["red", "blank"] } } )

db.inventory.find( { tags: "red" } )
db.inventory.find( { dim_cm: { $gt: 25 } } )
db.inventory.find( { dim_cm: { $gt: 15, $lt: 20 } } )

// Que cumpla al menos alguno
db.inventory.find( { dim_cm: { $elemMatch: { $gt: 22, $lt: 30 }}})

db.inventory.find( { "dim_cm.1": { $gt: 25 } } )

// tamaño
db.inventory.find( { "tags": { $size: 3 } } )
```

## Update and delete

```javascript
db.inventory.drop()
db.inventory.insertMany( [
   { item: "canvas", qty: 100, size: { h: 28, w: 35.5, uom: "cm" }, status: "A" },
   { item: "journal", qty: 25, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
   { item: "mat", qty: 85, size: { h: 27.9, w: 35.5, uom: "cm" }, status: "A" },
   { item: "mousepad", qty: 25, size: { h: 19, w: 22.85, uom: "cm" }, status: "P" },
   { item: "notebook", qty: 50, size: { h: 8.5, w: 11, uom: "in" }, status: "P" },
   { item: "paper", qty: 100, size: { h: 8.5, w: 11, uom: "in" }, status: "D" },
   { item: "planner", qty: 75, size: { h: 22.85, w: 30, uom: "cm" }, status: "D" },
   { item: "postcard", qty: 45, size: { h: 10, w: 15.25, uom: "cm" }, status: "A" },
   { item: "sketchbook", qty: 80, size: { h: 14, w: 21, uom: "cm" }, status: "A" },
   { item: "sketch pad", qty: 95, size: { h: 22.85, w: 30.5, uom: "cm" }, status: "A" }
] );

db.inventory.updateOne(
	{ item: "paper" },
	{$set: { "size.uom": "cm", status: "P" }, $currentDate: { lastModified: true }}
)

db.inventory.updateMany(
	{ "qty": { $lt: 50 } },
	{ $set: { "size.uom": "in", status: "P" }, $currentDate: {lastModified: true }}
)

//reemplaza todo menos el _id
db.inventory.replaceOne(
	{ item: "paper" },
	{ item: "paper", instock: [ { warehouse: "A", qty: 60 }, { warehouse: "B", qty: 40 } ] }
)

db.inventory.updateOne({noExistingField:1},{$set:{newField:"a new field"}},{upsert:true})
db.inventory.updateMany({},{$unset: {"size.uom":""}})

// En todos los documentos (multi), si no existe el campo lo crea
db.inventory.update({},{$inc:{number:1}},{multi:true} )

// Delete

db.inventory.deleteOne( { status: "D" } )
db.inventory.deleteMany({ status : "A" })
db.inventory.deleteMany({})

// drop collection
db.inventory.drop()
```

## Indexes

```javascript
peopleStats = people.explain("executionStats")
peopleStats.find({last_name : "Howard"})
people.createIndex({last_name:1})
peopleStats.find({last_name : "Howard"})

// Compound
people.createIndex({"address.state":1, "address.city":-1})
peopleStats.find({"address.state": "Missouri"})
peopleStats.find({"address.city": "Ryanville"})

// Multikey
db.inventory.insertMany([
   { item: "journal", qty: 25, tags: ["blank", "red"], dim_cm: [ 14, 21 ] },
   { item: "notebook", qty: 50, tags: ["red", "blank"], dim_cm: [ 14, 21 ] },
   { item: "paper", qty: 100, tags: ["red", "blank", "plain"], dim_cm: [ 14, 21 ] },
   { item: "planner", qty: 75, tags: ["blank", "red"], dim_cm: [ 22.85, 30 ] },
   { item: "postcard", qty: 45, tags: ["blue"], dim_cm: [ 10, 15.25 ] }
]);

db.inventory.createIndex({tags: 1})
db.inventory.explain("executionStats").find( { tags: { $in: ["red", "yellow"] } } )

// Equality, Sort, Range
people.createIndex({last_name:1, first_name: 1 , birthday:1})

peopleStats.find({last_name:"Howard", birthday: {$gte: ISODate("2012-04-29T00:00:00.000Z"),$lt: ISODate("2014-05-01T00:00:00.000Z") }}).sort({first_name: 1})

// Covered Queries
peopleStats.find({last_name:"Howard"}, {_id:0, last_name:1})

// Borramos la BD tests
use tests
db.dropDatabase()
show dbs
```

## Replica set

```zsh
// Create /data/n1 /data/n2 /data/n3 /logs
mongod --replSet repSetTest --dbpath /data/n1 --logpath /logs/n1.log --port 27000 --smallfiles --oplogSize 128 --fork
mongod --replSet repSetTest --dbpath /data/n2 --logpath /logs/n2.log --port 27001 --smallfiles --oplogSize 128 --fork
mongod --replSet repSetTest --dbpath /data/n3 --logpath /logs/n3.log --port 27002 --smallfiles --oplogSize 128 --fork

mongo localhost:27000
db.person.insert({name:'Fred', age:35})

cfg = {
	'_id':'repSetTest',
	'version': 1,
	'members':[
		{'_id':0, 'host': 'localhost:27000', priority: 1, votes: 1, hidden: false},
		{'_id':1, 'host': 'localhost:27001'},
		{'_id':2, 'host': 'localhost:27002', arbiterOnly: true}
	]
}

rs.initiate(cfg)
rs.status()

//connect to the primary
db.replTest.insert({_id:1, value:'abc'})
db.replTest.findOne()

//connect to the secondary
db.replTest.findOne()

// To can do queries on secondaries
rs.slaveOk(true)
db.replTest.findOne()
db.replTest.insert({_id:2, value:'abc'}, {w:'majority', wtimeout:100})

// ON primary
db.replTest.insert({_id:2, value:'abc'}, {w:'majority', wtimeout:100})
db.replTest.find({_id:2}).readPref('secondaryPreferred')

```

## Sharding

```zsh
// Replica for S1
mongod --replSet rsS1 --dbpath /data/s1/replA --port 27101 --fork --syslog
mongod --replSet rsS1 --dbpath /data/s1/replB --port 27102 --fork --syslog
mongod --replSet rsS1 --dbpath /data/s1/replC --port 27103 --fork --syslog

// Replica for S2
mongod --replSet rsS2 --dbpath /data/s2/replA --port 27201 --fork --syslog
mongod --replSet rsS2 --dbpath /data/s2/replB --port 27202 --fork --syslog
mongod --replSet rsS2 --dbpath /data/s2/replC --port 27203 --fork --syslog

// Replica for ConfigS

mongod --configsvr --replSet rsConf --dbpath /data/conf/replA --port 27301 --fork --syslog
mongod --configsvr --replSet rsConf --dbpath /data/conf/replB --port 27302 --fork --syslog
mongod --configsvr --replSet rsConf --dbpath /data/conf/replC --port 27303 --fork --syslog

// nos conectamos a un config server
mongo --port 27301

config = { _id: "rsConf",
			configsvr: true,
			members:[
				{ _id : 0, host : "localhost:27301" },
				{ _id : 1, host : "localhost:27302" },
				{ _id : 2, host : "localhost:27303" }
			]
		};
		
// Iniciamos el replica set.
rs.initiate(config)
exit

//Iniciamos el mongos
mongos --configdb rsConf/localhost:27301

// nos conectamos al mongos usando un script
mongo --shell ~/conf/Shard_with_replica.js

customAddShard("rsS1", 27101)
customAddShard("rsS2", 27201)
sh.status()
use tests
sh.enableSharding("tests")
sh.shardCollection('tests.people',{name:1, number:1});

var bulk = db.people.initializeUnorderedBulkOp();
people = ["Marc", "Bill", "George", "Eliot", "Matt", "Trey", "Tracy", "Greg", "Steve", "Kristina", "Katie", "Jeff"];
for(var i=0; i<50000; i++){
   user_id = i;
   name = people[Math.floor(Math.random()*people.length)];
   number = Math.floor(Math.random()*10001);
   bulk.insert( { "user_id":user_id, "name":name, "number":number });
}
bulk.execute();

use config
db.chunks.find({}, {min:1,max:1,shard:1,_id:0,ns:1})

db.people.find().explain()
```

## Instalando Robomongo

```zsh
tar -xvzf robo3t-1.1.1-linux-x86_64-c93c6b0.tar.gz
sudo mkdir /usr/local/bin/robomongo
sudo mv robo3t-1.1.1-linux-x86_64-c93c6b0/* /usr/local/bin/robomongo
cd /usr/local/bin/robomongo/bin
sudo chmod +x robo3t
sudo gedit ~/.zshrc

// Add this to the end of the file
alias robomongo="/usr/local/bin/robomongo/bin/robo3t"
// End

rm lib/libstdc++*
source ~/.zshrc

robomongo
```
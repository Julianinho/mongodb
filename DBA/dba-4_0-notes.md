# Mongo notes

1. [Introduction][intro]
2. [Mongo shell][shell]
3. [CRUD][crud]

[intro]: ./introduction.md
[shell]: ./shell.md
[crud]: ./crud.md
// Script de inicio del mongo shell.

// Declaración de variables
//var host = db.serverStatus().host;

// Definición del promt (NO EN WINDOWS)
//prompt = function() {
//             return db+"@"+host+"$ ";
			 //return "Uptime:"+db.serverStatus().uptime+" Documents:"+db.stats().objects+" > ";
//         }
		 
// Inicialización del replica set

var cfg = { 
	_id: 'ReplSet',
	//version:1,  // SOLO PARA INICIAR.
	protocolVersion:1, // default
	configsvr:false, // solo para sharded clusters con replica set
	writeConcernMajorityJournalDefault:true, // Default true para protocolversion =1 y false para protVer = 0. Setea por defecto un w: majority si no se especifica en la opción de journal.
    members: [
        { _id: 1, host: 'Julianinho-PC:27101'},
        { _id: 2, host: 'Julianinho-PC:27102'},
		{ _id: 3, host: 'Julianinho-PC:27103', priority:0.5},
		{ _id: 99, host: 'Julianinho-PC:27104', priority:0, hidden:true, slaveDelay: 8*3600}, // Obligatorio que sea sin prioridad para no ser elegido primario, recomendable que sea ocultado a los clientes y no acepte consultas y con un delay de 8 h.
        { _id: 69, host: 'Julianinho-PC:30000', arbiterOnly: true}
    ],
	settings: {
		chainingAllowed : false, // Puede replicar de secundarios. #Default: true
		heartbeatIntervalMillis : 10, // 10 secs, default
		heartbeatTimeoutSecs: 10,	// Default
		electionTimeoutMillis : 10000, // Default, 10 secs
		catchUpTimeoutMillis : 2000,	// Default
		//getLastErrorModes : { eastCoast: { "east": 1 } }, // funciona con TAGS
		getLastErrorDefaults : { w: "majority", wtimeout: 5000 }   //,
		//replicaSetId: <ObjectId>
	}
};

var error = rs.initiate(cfg);
//var error = rs.reconfig(cfg);
printjson(error);

		 
homework = { };

var db = connect('127.0.0.1:27017/tests');
var t = db.people;

homework.init = function() {
	
	if( t.count() ) { 
        throw "tests.people not empty, so won't init(). If you want to reinit, drop the collection first.";
    }

    var objToInsert = [
    { name: 'John', address: 'Highway 71', age: 53},
    { name: 'Peter', address: 'Lowstreet 4', age: 27},
    { name: 'Amy', address: 'Apple st 652', age: 15},
    { name: 'Hannah', address: 'Mountain 21', age: 38},
    { name: 'Michael', address: 'Valley 345', age: 11},
    { name: 'Sandy', address: 'Ocean blvd 2', age: 76},
    { name: 'Betty', address: 'Green Grass 1', age: 24},
    { name: 'Richard', address: 'Sky st 331', age: 33},
    { name: 'Susan', address: 'One way 98', age: 53},
    { name: 'Vicky', address: 'Yellow Garden 2', age: 26},
    { name: 'Ben', address: 'Park Lane 38', age: 35},
    { name: 'William', address: 'Central st 954', age: 41},
    { name: 'Chuck', address: 'Main Road 989', age: 2},
    { name: 'Viola', address: 'Sideway 1633', age: 91}
  ];

  t.insertMany(objToInsert, function(err, res) {
    if (err) throw err;
    print("Number of documents inserted: " + res.insertedCount);
    db.close();
  });
}

homework.close = function(){
	print("Closing server...");
	quit();
}

homework.delete = function(){
	print("Droping people collection...");
	db.people.drop();
	homework.close();
}

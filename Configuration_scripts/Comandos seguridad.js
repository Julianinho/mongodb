// AUTHENTICATION

// Autenticación interna entre miembros de un RSet (key file)
mongod --dbpath "/home/vagrant/M310-HW-1.2/r0" --logpath "/home/vagrant/M310-HW-1.2/r0/mongo.log" --keyFile ./mongodb-keyfile --port 31120 --replSet TO_BE_SECURED --fork

// Autenticación interna entre miembros de un RSet (X.509)
mongod --replSet TO_BE_SECURED --dbpath "/home/vagrant/M310-HW-1.3/r0" --logpath "/home/vagrant/M310-HW-1.3/r0/mongo.log" --port 31130 --fork --sslMode requireSSL --clusterAuthMode "x509" --sslPEMKeyFile "/home/vagrant/shared/certs/server.pem" --sslCAFile "/home/vagrant/shared/certs/ca.pem"
mongo --host "localhost:31130" --ssl --sslPEMKeyFile "/home/vagrant/shared/certs/client.pem" --sslCAFile "/home/vagrant/shared/certs/ca.pem"
use admin
rs.initiate({
 _id: 'TO_BE_SECURED',
 members: [
  { _id: 1, host: 'localhost:31130' },
  { _id: 2, host: 'localhost:31131' },
  { _id: 3, host: 'localhost:31132' }
 ]
})
db.createuser({user:'admin', pwd: 'admin', roles: ["root"]})
db.auth("admin","admin")
		// Este usuario debemos crearlo previamente al generar los ficheros
db.getSiblingDB("$external").runCommand({createUser: "C=US,ST=New York,L=New York City,O=MongoDB,OU=University2,CN=M310 Client", roles: [{role: 'root', db: 'admin'}] })
db = db.getSisterDB('$external')
db.auth({ mechanism: 'MONGODB-X509', user: 'C=US,ST=New York,L=New York City,O=MongoDB,OU=University2,CN=M310 Client'});


// Mixed authentication SCRAM-SHA + X509 auth client + key file internal

mongod --dbpath "/home/vagrant/M310-HW-1.5/r0" --port 31150 --replSet TO_BE_SECURED --fork --logpath "/home/vagrant/M310-HW-1.5/r0/mongo.log" --sslMode requireSSL --sslPEMKeyFile "/home/vagrant/shared/certs/server.pem" --sslCAFile "/home/vagrant/shared/certs/ca.pem" --keyFile ./mongodb-keyfile
mongo --host "localhost:31150" --ssl --sslPEMKeyFile "/home/vagrant/shared/certs/client.pem" --sslCAFile "/home/vagrant/shared/certs/ca.pem"
use admin
rs.initiate({
 _id: 'TO_BE_SECURED',
 members: [
  { _id: 1, host: 'localhost:31150' },
  { _id: 2, host: 'localhost:31151' },
  { _id: 3, host: 'localhost:31152' }
 ]
})
	//Creamos usuario SCRAM-SHA-1
db.createUser({user:'will', pwd: '$uperAdmin', roles: ["root"]})
db.auth("will","$uperAdmin")
	//Creamos usuario X509 con certificado
db.getSiblingDB("$external").runCommand({createUser: "C=US,ST=New York,L=New York City,O=MongoDB,OU=University2,CN=M310 Client", roles: [{role: 'userAdminAnyDatabase', db: 'admin'}] })
db.auth({ mechanism: 'MONGODB-X509', user: 'C=US,ST=New York,L=New York City,O=MongoDB,OU=University2,CN=M310 Client'});



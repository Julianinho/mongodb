mongod --port 27101 --dbpath D:\data\repSets\rs1\db --replSet ReplSet
mongod --port 27102 --dbpath D:\data\repSets\rs2\db --replSet ReplSet
mongod --port 27103 --dbpath D:\data\repSets\rs3\db --replSet ReplSet
mongod --port 27104 --dbpath D:\data\repSets\rs4\db --replSet ReplSet
mongod --port 30000 --dbpath D:\data\repSets\arbiter\db --replSet ReplSet
mongo --port 27101 mongoshell_repl_script.js --shell

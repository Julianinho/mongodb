# README

This README would normally document whatever steps are necessary to get your application up and running, or a brief resume of the contents.

## What is this repository for?

* Quick summary
This repo contains configuration files for replica sets, sharding, standalone instances. It contains too examples and scripts learned in the MongoDB courses and certifications.

* Version
1.0.0
	* Notes:
		- Adding configuration scripts
		- Adding yaml configuration
		- Adding mongoshell scripts
* Summary of set up
	1. Configuration scripts
	2. Mongo shell scripts
	3. yaml
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
